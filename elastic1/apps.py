# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class Elastic1Config(AppConfig):
    name = 'elastic1'
