from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import DocType, Text, Date

from .models import BlogPost

connections.create_connection()
#connections.create_connection()
class BlogPostIndex(DocType):
    author = Text()
    posted_date = Date()
    title = Text()
    text = Text()

    class Meta:
        index = 'blogpost-index'

from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch


def bulk_indexing():
    BlogPostIndex.init()
    es = Elasticsearch()
    bulk(client=es, actions=(b.indexing() for b in BlogPost.objects.all().iterator()))
